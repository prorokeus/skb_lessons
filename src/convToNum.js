export default function convToNum(str) {
	if (Number(str)) {
		const result = Number(str);

		return result;
	} else {
		return 0;
	}
}