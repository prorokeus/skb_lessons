import express from 'express';
import cors from 'cors';


import convToNum from './convToNum';
import convFullname from './convFullname';
const app = express();



app.use(cors());
app.get('/task2a', (req, res) => {
	const result = convToNum(req.query.a) + convToNum(req.query.b);
	res.send(result.toString());
});

app.get('/task2b', (req, res) => {
	const result = convFullname(req.query.fullname);
	res.send(result.toString());
});

app.listen(3000, () => {
	console.log('Your app listening on port 3000!');
});
